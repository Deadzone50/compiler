import java.util.Vector;

public class SymbolTable
{
	private Printer printer;
	private Vector<var> table;
	public SymbolTable()					//constructor
	{
		this.table = new Vector<var>();
	}
	public void printer(Printer printer)
	{
		this.printer = printer;
	}
	public int AddVar(var a)
	{
		if (CheckInST(a.name()) == -1)
		{
			//printer.print("ST added: " + a.name());
			a.SetPos(this.table.size());			//set the objects position in the symbol table
			this.table.add(a);
			return 1;
		}
		else
		{
			return -1;
		}
	}
	public int CheckInST(String name)
	{
		for(int i = 0;i < this.table.size();i++)
		{
			//printer.print("Looking for:"+name+", found:"+this.table.get(i).name());
			if( (this.table.get(i).name()).equals(name))
			{
				//printer.print("ST Found: "+name);
				return i;
			}
		}
		//printer.print("ST Not found: "+name);
		return -1;
	}
	public var Get(String name)
	{
		int pos = CheckInST(name);
		if (pos != -1)
		{
			//printer.print("ST Found: "+name);
			return this.table.get(pos);
		}
		else
		{
			//printer.print("ST Not found: "+name);
			return null;
		}
	}
	public void PrintST()
	{
		printer.print("SYMBOLTABLE:");
		for(int i = 0;i < this.table.size();i++)												//whole table
		{
			printer.print("#"+this.table.get(i).pos()+" "+this.table.get(i).name()+" "+this.table.get(i).ptype());			//print name + type
		}
	}
}
