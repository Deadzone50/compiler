import fi.tkk.cs.tkkcc.SlxCompiler;
import fi.tkk.cs.tkkcc.slx.Interpreter;
import fi.tkk.cs.tkkcc.slx.*;

import java.io.*;


public class Compiler implements SlxCompiler
{
    private Scanner scanner;
    private Parser parser;
    private CodeGenerator CG;

    public static void main(String[] args)
    {
        Compiler compiler = new Compiler();
        SlxProgram program = compiler.compile(args[0]);
        
        System.out.println("compiler.isErrors: " + (compiler.isErrors())); 
        if (program != null && !compiler.isErrors())
        {
        	// Run the program
        	Interpreter interpreter = new Interpreter(program, "all");
        	System.out.println("running");
					interpreter.execute(true, null);
        }
        
        return;
    }
    
    public Compiler()																			//empty constructor
    {
        return;
    }

    @Override
    public boolean isErrors()															//returns the number of errors encountered by the parser
    {
        return this.parser.errors.count > 0;
    }

    @Override
    public SlxProgram compile(String sourceFilename)			//compiles and returns the program
    {
			this.CG = new CodeGenerator();
			this.scanner = new Scanner(sourceFilename);
			this.parser = new Parser(this.scanner,  new Printer(false), this.CG);
			this.parser.Parse();
			SlxProgram prog = CG.output();
			return prog;
    }
}
