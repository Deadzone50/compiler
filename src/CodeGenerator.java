import java.io.*;

import fi.tkk.cs.tkkcc.slx.CommandWord;
import fi.tkk.cs.tkkcc.slx.SlxProgram;


public class CodeGenerator
{
		private SlxProgram prog;

    public CodeGenerator()			//constructor
    {
      prog = new SlxProgram();
    }
    
    public void PutOnStack(var obj)
    {
    	//~ if(!(obj.name().equals("temp")))		//if the object is not declared, the value is on top of stack stack (not used)
			//~ {
				this.emit("ENT",obj.pos());		//push address to top of stack
				this.emit("LDL");							//pop the address, and push the value at the address
			//~ }
						//value of obj is now on top of the stack
    }
    
    public int op(int operator,String type1,String type2)		//operator function
    {
			if(operator ==1)	// +
			{
				if((!(type1.equals("boolean")))&&(!(type2.equals("boolean"))))	//if both are int or read(unknown)
						this.emit("ADD");							//sum is on stack
				else
					return -1;
			}
			if(operator ==2)	// -
			{
				if((!(type1.equals("boolean")))&&(!(type2.equals("boolean"))))	//if both are int or read(unknown)
						this.emit("SUB");							//subtraction is on stack
				else
					return -1;
			}
			if(operator ==3)	// *
			{
				if((!(type1.equals("boolean")))&&(!(type2.equals("boolean"))))	//if both are int or read(unknown)
					this.emit("MUL");							//answer is on stack
				else
					return -1;
			}
			if(operator ==4)	// /
			{
				if((!(type1.equals("boolean")))&&(!(type2.equals("boolean"))))	//if both are int or read(unknown)
					this.emit("DIV");							//answer is on stack
				else
					return -1;
			}
			if(operator ==5)	// <
			{
				if((!(type1.equals("boolean")))&&(!(type2.equals("boolean"))))	//if both are int or read(unknown)
					this.emit("RLT");							//answer is on stack
				else
					return -1;
			}
			if(operator ==6)	// &&
			{
				if((!(type1.equals("int")))&&(!(type2.equals("int"))))	//if both are bool or read(unknown)
					this.emit("REQ");							//answer is on stack
				else
					return -1;
			}
			return 0;
		}
    
    public void emit(String word)
    {
    	CommandWord command = CommandWord.valueOf(word);
    	this.prog.emit(command);
    }
    public void emit(String word, int param1)
    {
    	CommandWord command = CommandWord.valueOf(word);
    	this.prog.emit(command,param1);
    }
    public void emit(String word,int param1,int param2)
    {
    	CommandWord command = CommandWord.valueOf(word);
    	this.prog.emit(command,param1,param2);
    }
    
    public SlxProgram output()		//used to get the program
    {
			return this.prog;
		}
    
}
