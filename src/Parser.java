

public class Parser {
	public static final int _EOF = 0;
	public static final int _identifier = 1;
	public static final int _number = 2;
	public static final int maxT = 29;

	static final boolean T = true;
	static final boolean x = false;
	static final int minErrDist = 2;

	public Token t;    // last recognized token
	public Token la;   // lookahead token
	int errDist = minErrDist;
	
	public Scanner scanner;
	public Errors errors;

	private Printer printer;
private SymbolTable ST;
private CodeGenerator CG;

public Parser(Scanner s, Printer p, CodeGenerator code)
{
	this(s);
	this.printer = p;
	this.ST = new SymbolTable();
	this.ST.printer(p);
	this.CG = code;
}



	public Parser(Scanner scanner) {
		this.scanner = scanner;
		errors = new Errors();
	}

	void SynErr (int n) {
		if (errDist >= minErrDist) errors.SynErr(la.line, la.col, n);
		errDist = 0;
	}

	public void SemErr (String msg) {
		if (errDist >= minErrDist) errors.SemErr(t.line, t.col, msg);
		errDist = 0;
	}
	
	void Get () {
		for (;;) {
			t = la;
			la = scanner.Scan();
			if (la.kind <= maxT) {
				++errDist;
				break;
			}

			la = t;
		}
	}
	
	void Expect (int n) {
		if (la.kind==n) Get(); else { SynErr(n); }
	}
	
	boolean StartOf (int s) {
		return set[s][la.kind];
	}
	
	void ExpectWeak (int n, int follow) {
		if (la.kind == n) Get();
		else {
			SynErr(n);
			while (!StartOf(follow)) Get();
		}
	}
	
	boolean WeakSeparator (int n, int syFol, int repFol) {
		int kind = la.kind;
		if (kind == n) { Get(); return true; }
		else if (StartOf(repFol)) return false;
		else {
			SynErr(n);
			while (!(set[syFol][kind] || set[repFol][kind] || set[0][kind])) {
				Get();
				kind = la.kind;
			}
			return StartOf(syFol);
		}
	}
	
	void Grammar() {
		printer.startProduction("Grammar"); 
		MainFuncDecl();
		Expect(0);
		printer.endProduction();												CG.emit("HLT");	//stop the program
																					
	}

	void MainFuncDecl() {
		printer.startProduction("MainFuncDecl"); 
		Expect(3);
		printer.print("main"); 
		FuncBody();
		printer.endProduction(); 
	}

	void FuncBody() {
		printer.startProduction("FuncBody"); 
		Expect(4);
		printer.print("{"); 
		if (la.kind == 21 || la.kind == 22) {
			VarDecl();
		}
		if (StartOf(1)) {
			StatementList();
		}
		ReturnStatement();
		Expect(5);
		printer.print("}"); printer.endProduction(); 
	}

	void VarDecl() {
		printer.startProduction("VarDecl");						String name, type; 
		type = Type();
		Expect(1);
		printer.print("identifier: "+t.val);						name = t.val; 
		Expect(6);
		printer.print(";");														var a = new var(name,type);		//add name to symbol table
																					if (ST.AddVar(a) == -1)
																					{
																						SemErr("Double declaration");
																					}
																					ST.PrintST(); 
		if (la.kind == 21 || la.kind == 22) {
			VarDecl();
		}
		printer.endProduction();	
	}

	void StatementList() {
		printer.startProduction("StatementList"); 
		Statement();
		if (StartOf(1)) {
			StatementList();
		}
		printer.endProduction(); 
	}

	void ReturnStatement() {
		printer.startProduction("ReturnStatement");		String type; 
		Expect(7);
		printer.print("return"); 
		type = Expr();
		Expect(6);
		printer.print(";"); printer.endProduction(); 	
	}

	String  Type() {
		String  type;
		printer.startProduction("Type");											type = "undef"; 
		if (la.kind == 21) {
			Get();
			printer.print("int"); printer.endProduction(); 			type = "int"; 
		} else if (la.kind == 22) {
			Get();
			printer.print("bool"); printer.endProduction(); 			type = "boolean"; 
		} else SynErr(30);
		return type;
	}

	String  Expr() {
		String  type;
		printer.startProduction("Expr");							int operator,op= 0;String type2 = null;type = null; 
		if (StartOf(2)) {
			type = BaseExpr();
			if (StartOf(3)) {
				operator = op();
				type2 = BaseExpr();
				op = CG.op(operator,type,type2);
				if (op == -1)															//error handling
				SemErr("different and or wrong types in operation");
																																									if ((operator == 5)||(operator == 6))			//type of value
				type = "boolean";
				else
				type = "int";
				
			}
			printer.endProduction(); 
		} else if (la.kind == 17) {
			Get();
			printer.print("!"); 
			type = BaseExpr();
			printer.endProduction(); 										if(!(type.equals("int")))							//if type is an boolean or a read value(unknown)
														{
															
															CG.emit("NOT");											//pop the value and push the inverted value
															
															type = "boolean";
														}
														else
														{
														SemErr("Wrong type for ! operator");
														} 
		} else SynErr(31);
		return type;
	}

	void Statement() {
		printer.startProduction("Statement"); 					String name;int value = 0;String type; 
		if (la.kind == 8) {
			Get();
			printer.print("if"); 
			Expect(9);
			type = Expr();
			Expect(10);
			Expect(11);
			if(type.equals("int"))			//if object is not boolean or read(unknown)
			{
			SemErr("boolean expected in if");
			}
																																						CG.emit("JZE", 1); 
			Statement();
			CG.emit("JMP", 2); 
			Expect(12);
			CG.emit("LAB", 1); 
			Statement();
			Expect(13);
			printer.print("fi"); printer.endProduction(); 	CG.emit("LAB",2); 
		} else if (la.kind == 14) {
			Get();
			printer.print("for"); 
			Expect(9);
			if (la.kind == 1) {
				ForStatement();
			}
			Expect(6);
			CG.emit("LAB", 3); 
			type = Expr();
			Expect(6);
			if(type.equals("int"))			//if object is not boolean or read(unknown)
			{
			SemErr("boolean expected in for condition");
			}
			CG.emit("JZE", 4);					//end of condition jump to end if false
			CG.emit("JMP", 5);					//jump to beginning of loop if true
			CG.emit("LAB", 6); 
			if (la.kind == 1) {
				ForStatement();
			}
			CG.emit("JMP", 3); 
			Expect(10);
			CG.emit("LAB", 5); 
			Statement();
			printer.endProduction();												CG.emit("JMP", 6);					//end of loop update statement
																			CG.emit("LAB", 4); 
		} else if (la.kind == 15) {
			Get();
			printer.print("print"); 
			Expect(9);
			type = Expr();
			Expect(10);
			Expect(6);
			printer.print(";"); printer.endProduction();		
																						CG.emit("WRI");					//pop the value and print it
																						 
		} else if (la.kind == 4) {
			Get();
			printer.print("{"); 
			StatementList();
			Expect(5);
			printer.print("}"); printer.endProduction(); 
		} else if (la.kind == 1) {
			ForStatement();
			Expect(6);
			printer.endProduction(); 
		} else SynErr(32);
	}

	void ForStatement() {
		printer.startProduction("ForStatement");	String name,type1 = null,type2 = null; 
		name = IdAccess();
		var Id = ST.Get(name);
		if(Id == null)
		{
		SemErr("Undeclared identifier");
		}
		else
		{
		CG.emit("ENT",Id.pos());				//put address of variable on stack
		type1 = Id.ptype();
		} 
		Expect(16);
		printer.print("<="); 
		type2 = Expr();
		printer.endProduction();									if(type1 != null)
												{
													if((type1.equals(type2))||(type2.equals("read")))						//check that they are same type or type 2 is read(unknown)
													{
														
														CG.emit("STL");									//store value at address
													}
													else
													{
														SemErr("Wrong type");
													}
												}
												
	}

	String  IdAccess() {
		String  name;
		printer.startProduction("IdAccess"); 
		Expect(1);
		printer.print("identifier: "+t.val);								name = t.val;
		printer.endProduction(); 
		return name;
	}

	String  BaseExpr() {
		String  type;
		printer.startProduction("BaseExpr");									String name; int value = 0;type = null; 
		switch (la.kind) {
		case 9: {
			Get();
			printer.print("("); 
			type = Expr();
			Expect(10);
			printer.print(")"); printer.endProduction(); 
			break;
		}
		case 1: {
			name = IdAccess();
			printer.endProduction(); 														var obj = ST.Get(name);								//get the right obj from the symbol table
																	if (obj == null)
																	{
																		SemErr("Undeclared identifier");
																	}
																	else
																	{
																		CG.PutOnStack(obj);
																		type = obj.ptype();
																	} 
			break;
		}
		case 2: {
			Get();
			printer.print("number: "+t.val); printer.endProduction();																		//TODO negative values?
																							try
																							{
																								value = Integer.parseInt(t.val);	//make the input an int
																							}
																							catch (NumberFormatException e)
																							{
																								SemErr("Too large int");
																							}
																							
																							type = "int";
																							CG.emit("ENT",value); 
			break;
		}
		case 18: {
			Get();
			printer.print("true"); printer.endProduction();			
																						type = "boolean";
																						CG.emit("ENT",1); 
			break;
		}
		case 19: {
			Get();
			printer.print("false"); printer.endProduction();	
																					type = "boolean";
																					CG.emit("ENT", 0); 
			break;
		}
		case 20: {
			Get();
			printer.print("read"); 	
			Expect(9);
			Expect(10);
			printer.endProduction();															
																								type = "read";
																								CG.emit("REA");										//read from stream to stack
																								 
			break;
		}
		default: SynErr(33); break;
		}
		return type;
	}

	int  op() {
		int  operator;
		printer.startProduction("op"); 											operator = 0;
		switch (la.kind) {
		case 23: {
			Get();
			printer.print("+"); printer.endProduction(); 				operator = 1; 
			break;
		}
		case 24: {
			Get();
			printer.print("-"); printer.endProduction(); 				operator = 2; 
			break;
		}
		case 25: {
			Get();
			printer.print("*"); printer.endProduction(); 				operator = 3; 
			break;
		}
		case 26: {
			Get();
			printer.print("/"); printer.endProduction(); 				operator = 4; 
			break;
		}
		case 27: {
			Get();
			printer.print("<"); printer.endProduction(); 				operator = 5; 
			break;
		}
		case 28: {
			Get();
			printer.print("&&"); printer.endProduction(); 				operator = 6; 
			break;
		}
		default: SynErr(34); break;
		}
		return operator;
	}



	public void Parse() {
		la = new Token();
		la.val = "";		
		Get();
		Grammar();
		Expect(0);

	}

	private static final boolean[][] set = {
		{T,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x},
		{x,T,x,x, T,x,x,x, T,x,x,x, x,x,T,T, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x},
		{x,T,T,x, x,x,x,x, x,T,x,x, x,x,x,x, x,x,T,T, T,x,x,x, x,x,x,x, x,x,x},
		{x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,x, x,x,x,T, T,T,T,T, T,x,x}

	};
} // end Parser


class Errors {
	public int count = 0;                                    // number of errors detected
	public java.io.PrintStream errorStream = System.out;     // error messages go to this stream
	public String errMsgFormat = "-- line {0} col {1}: {2}"; // 0=line, 1=column, 2=text
	
	protected void printMsg(int line, int column, String msg) {
		StringBuffer b = new StringBuffer(errMsgFormat);
		int pos = b.indexOf("{0}");
		if (pos >= 0) { b.delete(pos, pos+3); b.insert(pos, line); }
		pos = b.indexOf("{1}");
		if (pos >= 0) { b.delete(pos, pos+3); b.insert(pos, column); }
		pos = b.indexOf("{2}");
		if (pos >= 0) b.replace(pos, pos+3, msg);
		errorStream.println(b.toString());
	}
	
	public void SynErr (int line, int col, int n) {
		String s;
		switch (n) {
			case 0: s = "EOF expected"; break;
			case 1: s = "identifier expected"; break;
			case 2: s = "number expected"; break;
			case 3: s = "\"main\" expected"; break;
			case 4: s = "\"{\" expected"; break;
			case 5: s = "\"}\" expected"; break;
			case 6: s = "\";\" expected"; break;
			case 7: s = "\"return\" expected"; break;
			case 8: s = "\"if\" expected"; break;
			case 9: s = "\"(\" expected"; break;
			case 10: s = "\")\" expected"; break;
			case 11: s = "\"then\" expected"; break;
			case 12: s = "\"else\" expected"; break;
			case 13: s = "\"fi\" expected"; break;
			case 14: s = "\"for\" expected"; break;
			case 15: s = "\"print\" expected"; break;
			case 16: s = "\"<=\" expected"; break;
			case 17: s = "\"!\" expected"; break;
			case 18: s = "\"true\" expected"; break;
			case 19: s = "\"false\" expected"; break;
			case 20: s = "\"read\" expected"; break;
			case 21: s = "\"int\" expected"; break;
			case 22: s = "\"boolean\" expected"; break;
			case 23: s = "\"+\" expected"; break;
			case 24: s = "\"-\" expected"; break;
			case 25: s = "\"*\" expected"; break;
			case 26: s = "\"/\" expected"; break;
			case 27: s = "\"<\" expected"; break;
			case 28: s = "\"&&\" expected"; break;
			case 29: s = "??? expected"; break;
			case 30: s = "invalid Type"; break;
			case 31: s = "invalid Expr"; break;
			case 32: s = "invalid Statement"; break;
			case 33: s = "invalid BaseExpr"; break;
			case 34: s = "invalid op"; break;
			default: s = "error " + n; break;
		}
		printMsg(line, col, s);
		count++;
	}

	public void SemErr (int line, int col, String s) {	
		printMsg(line, col, s);
		count++;
	}
	
	public void SemErr (String s) {
		errorStream.println(s);
		count++;
	}
	
	public void Warning (int line, int col, String s) {	
		printMsg(line, col, s);
	}
	
	public void Warning (String s) {
		errorStream.println(s);
	}
} // Errors


class FatalError extends RuntimeException {
	public static final long serialVersionUID = 1L;
	public FatalError(String s) { super(s); }
}
