public class var									//variable class
{
	private String var_name;
	private int var_type;						//"int"=0,"bool"=1 "read" = 2
	private int pos = 0;
	
	public var(String name, String type)		//constructor
	{
		this.var_name = name;
		if(type.equals("int"))
			this.var_type = 0;
		if(type.equals("boolean"))
			this.var_type = 1;
		if(type.equals("read"))
			this.var_type = 2;
	}
	
	public String name()			//return name
	{
		return this.var_name;
	}
	
	public int type()					//return type as int
	{
		return this.var_type;
	}
	
	public String ptype()			//return type as String
	{
		if(this.var_type == 0)
			return "int";
		if(this.var_type == 1)
			return "boolean";
		if(this.var_type == 2)
			return "read";
		else
			return "unknown";
	}
	
	public void SetPos(int i)	//set position in symbol table
	{
		this.pos = i;
	}
	
	public int pos()					//return position in symbol table
	{
		return this.pos;
	}
}
